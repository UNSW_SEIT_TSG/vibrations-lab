# vibrations-lab

## Setup

### Install Anaconda (Optional)
https://repo.anaconda.com/archive/Anaconda3-2020.07-Windows-x86_64.exe

### Install Visual Studio Code (Optional)
https://code.visualstudio.com/download

### Set up gitlab account (Optional)
https://gitlab.com/users/sign_up

### Install git
https://git-scm.com/download/win

### Install node version manager
https://github.com/coreybutler/nvm-windows/releases

### Install node
From your terminal run the command: `nvm install 14`
Check that it worked: `node --version`

### Install nodemon globally
From your terminal run the command: `npm install -g nodemon`

### Set up ssh key
If registered with gitlab it is possible to use gitlab via ssh using an ssh key.
To set up an ssh key navigate to https://gitlab.com/profile/keys.

* Generate the key https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair
* Copy the generated key from the computer to gitlab https://gitlab.com/help/ssh/README#review-existing-ssh-keys

### Clone repository
From a suitable directory run the command: `git clone git@gitlab.com:UNSW_SEIT_TSG/vibrations-lab.git`

### Install vibrations-lab
From root directory of repository run command: `npm i`

npm install -g nodemon


## Execution
### Launch web server
`node app.js`

### Launch simulation
There are currently 2 working experiments:
* linear_experiment_a.js served on http://localhost:7000/linear_experiment_a.html
* linear_experiment_b.js served on http://localhost:7000/linear_experiment_b.html

to launch either experiment execute the command:
`node linear_experiment_a.js`
or
`node linear_experiment_b.js`

If dynamic restarts are desired `node` can be replaced with `nodemon`