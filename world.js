const World = chronon => {
  const bodies = {}
  const dampers = {}
  const free_bodies = []
  const springs = {}
  const ids = new Set()

  const validateIdentifier = identifier => {
    if(!ids.has(identifier)){
      ids.add(identifier)
    }else{
      throw new Error(`identifier duplication detected: ${identifier}`)
    }
  }
  
  const Body = ({ id, m=1, x=0, v=0, a=0, F=0, fixed=false}) => {
    validateIdentifier(id)
    const springs=[]
    const dampers=[]
    const out = { id, type: 'Body', m, x, v, a, F, fixed, springs, dampers }
    
    bodies[id] = out
    if (!fixed){
      free_bodies.push(out)
    }
    return out
  }
  
  const Damper = ({anchor_a=frame.id , anchor_b=frame.id, damping=0}) => {
    ;[anchor_a, anchor_b].map(anchor => {
      if (!ids.has(anchor)){
        throw new Error(
          `identifier '${anchor}' not registered in set: ${Object.values(ids)}`
        )
      }
    })
  
    const id = `c_${[anchor_a, anchor_b].sort().join("_")}`
    validateIdentifier(id)

    bodies[anchor_a].dampers.push(id)
    bodies[anchor_b].dampers.push(id)

    const out = {id, type:'Damper', anchor_a, anchor_b, damping}
    dampers[id] = out
    return out
  }
  
  const Spring = ({anchor_a=frame.id , anchor_b=frame.id, stiffness=1, rest_length=2}) => {
    ;[anchor_a, anchor_b].map(anchor => {
      if (!ids.has(anchor)){
        throw new Error(
          `identifier '${anchor}' not registered in set: ${Object.values(ids)}`
        )
      }
    })
  
    const id = `k_${[anchor_a, anchor_b].sort().join("_")}`
    validateIdentifier(id)

    bodies[anchor_a].springs.push(id)
    bodies[anchor_b].springs.push(id)
      
    const out = {id, type:'Spring', anchor_a, anchor_b, stiffness, rest_length}
    springs[id] = out
    return out
  }

  const calculateSpringForces = free_body => {
    // Calculate forces exerted on a free body by connected springs
    free_body.springs.forEach(spring_id => {
      const spring = springs[spring_id]
      const x_a = bodies[spring.anchor_a].x 
      const x_b = bodies[spring.anchor_b].x
      const k   = spring.stiffness
      const s_0 = spring.rest_length
      const s   = x_b - x_a
      const δ   = s - s_0
      
      const x_spring_centre = (x_b + x_a)/2
      
      if (free_body.x > x_spring_centre){
        free_body.F += k*δ
      }else{
        free_body.F -= k*δ
      }
    })
  }

  const calculateDamperForces = free_body => {
    // Calculate forces exerted on a free body by connected dampers
    free_body.dampers.forEach(damper_id => {
      const damper = dampers[damper_id]
      const c   = damper.damping
      const v   = free_body.v
      free_body.F += c*v
    })
  }

  const calculateForces = () => {
    free_bodies.forEach(free_body => {
      calculateSpringForces(free_body)
      calculateDamperForces(free_body)
    })
  }

  const updateFreeBody = free_body => {
    // Applies a force to a free body thereby consuming the force
    free_body.a  = -free_body.F/free_body.m
    free_body.F  = 0

    // Update velocity according to new acceleration
    free_body.v += free_body.a*chronon

    // Update position according to new velocity
    free_body.x += free_body.v*chronon
  }

  const update = () => {
    calculateForces()
    updates = {}

    free_bodies.forEach(free_body => {
      updateFreeBody(free_body)

      if (free_body.x > 5 || free_body.x < -5){
        console.log(`x out of bounds:`)
        console.log(free_body)
        throw Error
      }

      updates[free_body.id] = {
        x: free_body.x,
        v: free_body.v,
        a: free_body.a
      }
    })
    return updates
  }

  const getConfiguration = () => {
    const configuration = {}
    Object.keys(bodies).forEach(k => {
      if (!bodies[k].fixed){
        configuration[k] = bodies[k].m
      }
    })
    Object.keys(dampers).forEach(k => configuration[k] = dampers[k].damping)
    Object.keys(springs).forEach(k => configuration[k] = springs[k].stiffness)

    return configuration
  }

  return {Body, Damper, Spring, getConfiguration, update}
}

module.exports = World