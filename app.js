const http = require("http");
const path = require("path");
const express = require("express");
const WebSocket = require('ws');

process.title = "networked-aframe-server";
const port = process.env.PORT || 7000;

let sockets = []


// Setup and configure Express http server.
const app = express();
app.use(express.static('public'));

// Start Express http server
const webServer = http.createServer(app);
const io = require("socket.io")(webServer);

const rooms = {};

io.on("connection", socket => {
  sockets.push(socket)

  console.log("user connected", socket.id);

  let curRoom = null;

  socket.on("joinRoom", data => {
    const { room } = data;

    if (!rooms[room]) {
      rooms[room] = {
        name: room,
        occupants: {},
      };
    }

    const joinedTime = Date.now();
    rooms[room].occupants[socket.id] = joinedTime;
    curRoom = room;

    console.log(`${socket.id} joined room ${room}`);
    socket.join(room);

    socket.emit("connectSuccess", { joinedTime });
    const occupants = rooms[room].occupants;
    io.in(curRoom).emit("occupantsChanged", { occupants });
  });

  socket.on("send", data => {
    console.log(`sending ${data}`)
    io.to(data.to).emit("send", data);
  });

  socket.on("broadcast", data => {
    // console.log(`broadcast ${data}`)
    socket.to(curRoom).broadcast.emit("broadcast", data);
  });

  socket.on("disconnect", () => {
    console.log('disconnected: ', socket.id, curRoom);
    if (rooms[curRoom]) {
      console.log("user disconnected", socket.id);

      delete rooms[curRoom].occupants[socket.id];
      const occupants = rooms[curRoom].occupants;
      socket.to(curRoom).broadcast.emit("occupantsChanged", { occupants });

      if (occupants == {}) {
        console.log("everybody left room");
        delete rooms[curRoom];
      }
    }
    sockets = sockets.filter(x => x != socket)
  });
});

webServer.listen(port, () => {
  console.log("listening on http://localhost:" + port);
});

const wss = new WebSocket.Server({ port: 8081 });

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    msg = JSON.parse(message)
    console.log(msg.tag);
    console.log(msg.values);
    
    sockets.forEach(socket => {
      socket.emit(msg.tag || 'update', msg.values)
    });
  });

  ws.send('connected');
});