const WebSocket = require('ws');
const ws = new WebSocket('ws://localhost:8081');
const World = require('./world')

// 3 mass 4 linear spring system experiment parameters
// This experiment is the same as linear experiment a, except for an additional
// spring bypassing the green mass; i.e. linking blue directly to red.

chronon = 0.01

const {Body, Damper, Spring, getConfiguration, update} = World(chronon)

const wall_left  = Body({id:'wall_left',  x: -4.0, fixed: true})
const body_blue  = Body({id:'body_blue',  x: -2.0 })
const body_green = Body({id:'body_green', x: -0.5 })
const body_red   = Body({id:'body_red',   x:  1.5 })
const wall_right = Body({id:'wall_right', x:  4.0, fixed: true})

Damper({anchor_a: wall_left.id,  anchor_b: body_blue.id,  damping: 0.01})
Damper({anchor_a: wall_left.id,  anchor_b: body_green.id, damping: 0.01})
Damper({anchor_a: wall_left.id,  anchor_b: body_red.id,   damping: 0.01})

Spring({anchor_a: wall_left.id,  anchor_b: body_blue.id})
Spring({anchor_a: body_blue.id,  anchor_b: body_green.id})
Spring({anchor_a: body_blue.id,  anchor_b: body_red.id})
Spring({anchor_a: body_green.id, anchor_b: body_red.id})
Spring({anchor_a: body_red.id,   anchor_b: wall_right.id})
// End of experiment parameters

ws.on('open', function open() {
  ws.send(JSON.stringify({
    // Constant values, only sent when initially connected
    tag: 'parameters',
    values: getConfiguration()
  }))
});

ws.on('message', function incoming(data) {
  console.log(data);
});

const φ = () => {
  updates = update() // Next step of physics simulation, increment t by chronon
  
  // Send state of simulation to web viewer
  ws.send(JSON.stringify({tag: 'update', values: updates}))
}

setInterval(φ, chronon*1000) // 1 frame every chronon*1000 ms
