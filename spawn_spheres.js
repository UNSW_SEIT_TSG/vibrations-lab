// Spawn spheres
function Sphere({ position="0 0 0" }){
  var sphere = document.createElement('a-sphere')
  sphere.setAttribute('position', position)
  sphere.setAttribute('random-color')
  return sphere
}

var scene = document.getElementById('system-scene')
scene.addChild(Sphere())

// add 10 spheres
Array(10).fill(0).map(Sphere).forEach(
  el => scene.addChild(el)
)